import sys

if __name__ == "__main__":

    my_nb = int(sys.argv[1])

    for elt in range(0, my_nb):
        print(elt, " ", int(elt**2))
        sleep(1)